'use strict';
//--< ============================================================== Константы ====================================================================
const baseUrl = 'http://localhost:7272';
//--< ***************** Чат *****************************
function User(sessionNumber, nickname) {
    this.sessionNumber = sessionNumber;
    this.nickname = nickname;
}

function Message(text, datetime, user) {
    this.text = text;
    this.datetime = datetime;
    this.user = user;
}
function createMessageElement(message) {
    let newElement = document.createElement('div');
    newElement.className = 'message_item';
    newElement.innerHTML =
        `<div class="message_item_block">`+
        `<p class="message_user_login">${message.user.nickname}</p>`+
        `<p class="message_time">${message.datetime}</p>`+
        `</div>`+
        `<p class="message_text">${message.text}</p>`
    ;
    return newElement;
}
async function fetchGetMessages() {
    await fetch("/messages").then(res => {
        if (res.ok){
            res.json().then(data => {
                if (data !== []){
                    for (let i = 0; i < data.length; i++) {
                        if (document.getElementsByClassName('message_item')[0] !== undefined){
                            document.getElementsByClassName('message_item')[document.getElementsByClassName('message_item').length-1].after(
                                createMessageElement(new Message(data[i].text, data[i].datetime, new User (data[i].user.sessionNumber, data[i].user.nickname)))
                            );
                            document.getElementById('chat_block').scrollTop = document.getElementById('chat_block').scrollHeight;
                        } else {
                            addMessage(createMessageElement(new Message(data[i].text, data[i].datetime, new User (data[i].user.sessionNumber, data[i].user.nickname))));
                            document.getElementById('chat_block').scrollTop = document.getElementById('chat_block').scrollHeight;
                        }

                    }
                }
            }).catch(err => {})
        } else {
        }

    });

}
function addMessage(element){
    document.getElementById("chat_block").append(element);
}


async function onSendMessageHandler(e){
    e.preventDefault();
    e.stopPropagation();
    const form = document.getElementById("message_form");
    const data = new FormData(form);
    const dataJSON = JSON.stringify(Object.fromEntries(data));
    const options = {
        method : 'post',
        headers: {
            'Content-Type': 'application/json'
        },
        body: dataJSON
    };
    await fetch("/messages", options);
}
document.getElementById('message_form_btn').addEventListener('click', onSendMessageHandler);
setInterval(fetchGetMessages, 1000);

//--> ***************** Чат *****************************