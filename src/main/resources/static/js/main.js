'use strict';
//--< ============================================================== Константы ====================================================================
const baseUrl = 'http://localhost:7272';


//--< ***************** Создание сессии *****************************
async function fetchAddSession(e) {
    e.preventDefault();
    e.stopPropagation();
    const form = document.getElementById("session_form");
    const data = new FormData(form);
    const dataJSON = JSON.stringify(Object.fromEntries(data));
    const options = {
        method : 'post',
        headers: {
            'Content-Type': 'application/json'
        },
        body: dataJSON
    };

    await fetch("/users", options);
}
if (document.getElementsByClassName("session_form_btn")[0] !== undefined) {
    document.getElementsByClassName("session_form_btn")[0].addEventListener('click', function (e) {
        fetchAddSession(e).then(res => {
            window.location.href = `${baseUrl}/chat`;

        });
    });
}

//--> ***************** Создание сессии *****************************
