package homework72.multiplayerchat.utils;

import homework72.multiplayerchat.backend.repository.Messages;
import homework72.multiplayerchat.backend.repository.Users;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

@Configuration
public class PreloadDatabaseWithData {
    private static final Random rn = new Random();
    @Bean
    CommandLineRunner clearDatabase(Users users, Messages messages){
        return (args) -> {
            users.deleteAll();
            messages.deleteAll();
        };


    }
}
