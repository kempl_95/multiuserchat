package homework72.multiplayerchat.backend.controller;

import homework72.multiplayerchat.backend.model.Message;
import homework72.multiplayerchat.backend.model.User;
import homework72.multiplayerchat.backend.service.UserService;
import homework72.multiplayerchat.dto.MessageDTO;
import homework72.multiplayerchat.dto.frontDTO.NewMessageDTO;
import homework72.multiplayerchat.dto.frontDTO.NewUserDTO;
import homework72.multiplayerchat.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    UserService userService;

    @PostMapping
    public void addUser(@RequestBody NewUserDTO newUserDTO, HttpSession session) {
        if (session != null) {
            if (session.getAttribute(Constants.USER_KEY) == null) {
                var user = User.builder()
                        .nickname(newUserDTO.getNickname())
                        .sessionNumber(session.getId())
                        .cnt(0)
                        .build();

                session.setAttribute(Constants.USER_KEY, user);
                userService.addUser(user);
            }
        }
    }
}
