package homework72.multiplayerchat.backend.controller;

import homework72.multiplayerchat.backend.model.Message;
import homework72.multiplayerchat.backend.model.User;
import homework72.multiplayerchat.backend.service.MessageService;
import homework72.multiplayerchat.backend.service.UserService;
import homework72.multiplayerchat.dto.MessageDTO;
import homework72.multiplayerchat.dto.frontDTO.NewMessageDTO;
import homework72.multiplayerchat.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping("/messages")
public class MessageController {
    @Autowired
    MessageService messageService;

    @Autowired
    UserService userService;

    @GetMapping
    public List<MessageDTO> getMessages(HttpSession session) {
        if (session.getAttribute(Constants.MESSAGE_KEY) == null){
            session.setAttribute(Constants.MESSAGE_KEY, new ArrayList<Message>());
        }
        var list = messageService.findAll();
        var user = userService.getUserBySessionNumber(session.getId());
        if (list.size() != 0){
            if (user.getCnt() != list.size()){
                int idx = list.size() - user.getCnt();
                var messageList = new ArrayList<Message>();
                for (int i=idx; i>0;i--){
                    var newMessage = list.get(list.size()-(i));
                    messageList.add(newMessage);
                }
                user.setCnt(list.size());
                userService.saveCnt(user);
                return MessageDTO.listFrom(messageList);
            } else
                return null;
        } else{
            return null;
        }
    }
    @PostMapping
    public void addMessage(@RequestBody NewMessageDTO newMessageDTO, HttpSession session){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        if (session != null) {
            if (session.getAttribute(Constants.MESSAGE_KEY) == null){
                session.setAttribute(Constants.MESSAGE_KEY, new ArrayList<Message>());
            }
            var list = (List<Message>) session.getAttribute(Constants.MESSAGE_KEY);
            var user = (User)session.getAttribute(Constants.USER_KEY);
            var message = Message.builder()
                    .text(newMessageDTO.getText())
                    .dateTime(LocalDateTime.now().format(formatter))
                    .user(user)
                    .build();
            list.add(message);
            session.setAttribute(Constants.MESSAGE_KEY, list);
            messageService.addMessage(message);
        }
    }
}
