package homework72.multiplayerchat.backend.service;

import homework72.multiplayerchat.backend.model.User;
import homework72.multiplayerchat.backend.repository.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    @Autowired
    Users users;

    public void addUser(User user) {
        users.save(user);
    }

    public User getUserBySessionNumber(String number) {
        return users.getBySessionNumber(number);
    }
    public void saveCnt(User user){
        var user2 = users.getBySessionNumber(user.getSessionNumber());
        user2.setCnt(user.getCnt());
        users.save(user2);
    }

}
