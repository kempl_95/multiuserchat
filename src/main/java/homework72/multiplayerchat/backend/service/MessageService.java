package homework72.multiplayerchat.backend.service;

import homework72.multiplayerchat.backend.model.Message;
import homework72.multiplayerchat.backend.model.User;
import homework72.multiplayerchat.backend.repository.Messages;
import homework72.multiplayerchat.backend.repository.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MessageService {
    @Autowired
    Messages messages;

    public void addMessage(Message message) {
        messages.save(message);
    }

    public List<Message> findAll() {
        return messages.findAll();
    }

}
