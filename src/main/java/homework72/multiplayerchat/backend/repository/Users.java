package homework72.multiplayerchat.backend.repository;

import homework72.multiplayerchat.backend.model.Message;
import homework72.multiplayerchat.backend.model.User;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;


public interface Users extends PagingAndSortingRepository<User, String> {
    User getBySessionNumber(String number);

}
