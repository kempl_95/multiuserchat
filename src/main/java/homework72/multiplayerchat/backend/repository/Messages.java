package homework72.multiplayerchat.backend.repository;

import homework72.multiplayerchat.backend.model.Message;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface Messages extends PagingAndSortingRepository<Message, String> {
    List<Message> findAll();
}
