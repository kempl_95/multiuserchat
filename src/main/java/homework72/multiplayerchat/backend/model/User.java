package homework72.multiplayerchat.backend.model;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor
@Document(collection = "users")
public class User {
    @Id
    private String id;
    @Getter
    @Setter
    private String sessionNumber;
    @Getter
    @Setter
    private String nickname;
    @Getter
    @Setter
    private int cnt;
}
