package homework72.multiplayerchat.backend.model;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor
@Document(collection = "messages")
public class Message {
    @Id
    private String id;
    @Getter
    @Setter
    private String text;
    @Getter
    @Setter
    private String dateTime;
    @Getter
    @Setter
    private User user;
}
