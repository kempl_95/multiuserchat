package homework72.multiplayerchat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//@EnableMongoRepositories(basePackageClasses = MultiplayerchatApplication.class)
public class MultiplayerchatApplication {

    public static void main(String[] args) {
        SpringApplication.run(MultiplayerchatApplication.class, args);
    }

}
