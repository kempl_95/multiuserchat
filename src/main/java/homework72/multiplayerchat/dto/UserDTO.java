package homework72.multiplayerchat.dto;

import homework72.multiplayerchat.backend.model.User;
import lombok.*;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class UserDTO {

    public static UserDTO from(User user) {
        return builder()
                .sessionNumber(user.getSessionNumber())
                .nickname(user.getNickname())
                .build();
    }

    private String sessionNumber;
    private String nickname;

}
