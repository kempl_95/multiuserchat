package homework72.multiplayerchat.dto.frontDTO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NewUserDTO {
    private String nickname;
}
