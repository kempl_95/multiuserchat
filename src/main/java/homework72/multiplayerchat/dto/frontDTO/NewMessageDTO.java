package homework72.multiplayerchat.dto.frontDTO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NewMessageDTO {
    private String text;
}
