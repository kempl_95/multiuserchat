package homework72.multiplayerchat.dto;

import homework72.multiplayerchat.backend.model.Message;
import homework72.multiplayerchat.backend.model.User;
import lombok.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class MessageDTO {
    public static MessageDTO from(Message message) {
        return builder()
                .text(message.getText())
                .datetime(message.getDateTime().toString())
                .user(message.getUser())
                .build();
    }
    public static List<MessageDTO> listFrom(List<Message> objList){
        List<MessageDTO> listDto = new ArrayList<>();
        objList.stream().forEach(obj -> {
            listDto.add(MessageDTO.from(obj));
        });
        return listDto;
    }

    private Integer id;
    private String text;
    private String datetime;
    private User user;
}
