package homework72.multiplayerchat.frontend;
import homework72.multiplayerchat.backend.model.Message;
import homework72.multiplayerchat.backend.model.User;
import homework72.multiplayerchat.dto.MessageDTO;
import homework72.multiplayerchat.dto.frontDTO.NewMessageDTO;
import homework72.multiplayerchat.dto.frontDTO.NewUserDTO;
import homework72.multiplayerchat.utils.Constants;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import java.security.Principal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping
@AllArgsConstructor
public class FrontendController {

    @GetMapping
    public String addUserGet() {
        return "enter";
    }

    @GetMapping("/chat")
    public String chatPage() {
        return "chat";
    }




}
